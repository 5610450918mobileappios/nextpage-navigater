//
//  Page2ViewController.swift
//  nextpage
//
//  Created by Teerapat on 9/15/2558 BE.
//  Copyright (c) 2558 Teerapat. All rights reserved.
//

import UIKit

class Page2ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setText.text = text
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let vc = segue.destinationViewController as! Page3ViewController
        vc.text = "Hi"
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    @IBAction func nextPage3(sender: AnyObject) {
        performSegueWithIdentifier("toPage3", sender: self)
    }
    @IBAction func unwindPage2(seg:UIStoryboardSegue!){
        
    }
    @IBAction func backToPage1(sender: AnyObject) {
        performSegueWithIdentifier("unwindPage1", sender: self)
    }

    @IBOutlet weak var setText: UITextField!
    var text:String!
}
