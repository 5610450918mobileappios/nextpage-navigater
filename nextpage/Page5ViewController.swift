//
//  Page5ViewController.swift
//  nextpage
//
//  Created by Teerapat on 9/15/2558 BE.
//  Copyright (c) 2558 Teerapat. All rights reserved.
//

import UIKit

class Page5ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func backButtonTouch(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func backToFirstButtonTouch(sender: AnyObject) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    @IBAction func backTwoPagesButtonTouch(sender: AnyObject) {
        if let vcs = self.navigationController?.viewControllers{
            let vc:UIViewController = vcs[0] as! Page2ViewController
            
            self.navigationController?.popToViewController(vc, animated: true)
        }
    }

}
