//
//  ViewController.swift
//  nextpage
//
//  Created by Teerapat on 9/15/2558 BE.
//  Copyright (c) 2558 Teerapat. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func nextPageManualTough(sender: AnyObject) {
        performSegueWithIdentifier("toPage2", sender: self)
    }
    @IBAction func nextPageManual2Touch(sender: AnyObject) {
        let vc = self.storyboard?.instantiateViewControllerWithIdentifier("Page2") as! UIViewController
        presentViewController(vc, animated: true, completion: nil)
    }
    @IBAction func unwindPage1(seg:UIStoryboardSegue!){
        
    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let vc = segue.destinationViewController as! Page2ViewController
        vc.text = "Hi"
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    }

